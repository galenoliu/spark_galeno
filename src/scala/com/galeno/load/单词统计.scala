package com.galeno.load

import org.apache.spark.{SparkConf, SparkContext}

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/8/259:32
 */
object 单词统计 extends App {
  private val conf = new SparkConf
  conf.setAppName(this.getClass.getName)
  conf.setMaster("local[*]")
  private val context = new SparkContext(conf)
  context.textFile("data/wordcount.txt")
    .flatMap(line=>line.split("\\s+"))
    .groupBy(word=>word)
    .mapValues(count=>count.size)
    .sortBy(- _._2)
    .foreach(println)
}
