package com.galeno.练习

import com.galeno.utils.SparkUtil
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/8/2619:58
 */
object cogroup算子练习 {
  def main(args: Array[String]): Unit = {
    // TODO 利用 cogroup 算子 实现上述两个rdd的： 内join效果， 外join效果
    SparkUtil.noLog()
    val sc = SparkUtil.getSc
    val rdd1 = sc.makeRDD(Seq(("a",1 ), ("a",10), ("b",2), ("e",10)))
    val rdd2 = sc.makeRDD(Seq(("a","10" ), ("a","80" ),("a","60" ),("b","20"), ("c","20")))
    val value: RDD[(String, (Iterable[Int], Iterable[String]))] = rdd1.cogroup(rdd2)
    value.foreach(println)
    val joinIn: RDD[(String, (Int, String))] = rdd1.join(rdd2)
    println("*"*100)
    joinIn.foreach(println)
    println("*"*100)







  }

}
