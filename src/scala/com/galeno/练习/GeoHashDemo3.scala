package com.galeno.练习

import ch.hsr.geohash.GeoHash
import com.galeno.utils.SparkUtil
import org.apache.spark.rdd.{JdbcRDD, RDD}

import java.sql.{DriverManager, ResultSet}

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/9/221:37
 */
object GeoHashDemo3 {
  def main(args: Array[String]): Unit = {
    val sc = SparkUtil.getSc

    val logRdd: RDD[String] = sc.textFile("data/app_log_2021-06-05.log")

    //读取MySQL
    var getConn = () => DriverManager.getConnection("jdbc:mysql://localhost:3306/spark", "root", "root")
    var sql = "select lat,lng,province,city,region from ref_zb where lat>=? and lat<=?"
    var mapRow = (rs: ResultSet) => {
      val lat = rs.getDouble(1)
      val lng = rs.getDouble(2)
      val province = rs.getString(3)
      val city: String = rs.getString(4)
      val region = rs.getString(5)
      val geohash = GeoHash.geoHashStringWithCharacterPrecision(lat, lng, 6)

      (geohash, province, city, region)
    }
    val jdbcRdd: JdbcRDD[(String, String, String, String)] = new JdbcRDD[(String, String, String, String)](sc, getConn, sql, -90, 90, 1, mapRow)

    jdbcRdd.map(_.productIterator.mkString(",")).saveAsTextFile("data/ref_geohash")

    sc.stop()





  }
}
