package com.galeno.练习

import ch.hsr.geohash.GeoHash

import java.sql.DriverManager

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/8/289:43
 */
object DemoMap {
  def main(args: Array[String]): Unit = {
    val str: String = GeoHash.geoHashStringWithCharacterPrecision(30.598755982100613, 114.2961261186026, 12)
    val str1: String = GeoHash.geoHashStringWithCharacterPrecision(30.598755, 114.296126125, 11)
   println(str1)
    println(str)
    val con = DriverManager.getConnection("jdbc:mysql://localhost:3306/spark","root","root")
    val stmt = con.prepareStatement("select  BD09_LNG ,BD09_LAT FROM area")
    val rs = stmt.executeQuery()
    while (rs.next()){
      val jingdu = rs.getString("BD09_LNG")
      val weidu = rs.getString(  "BD09_LAT")
      //println(jingdu,weidu)
      if (jingdu!=null&&weidu!=null){
        val str2: String = GeoHash.geoHashStringWithCharacterPrecision(weidu.toDouble, jingdu.toDouble,12)
        println(str2)
      }
    }



  }


}
