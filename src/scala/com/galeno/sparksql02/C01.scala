package com.galeno.sparksql02

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.types.{DataTypes, StructField, StructType}

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/9/510:20
 */
object C01 {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("")
      .master("local")
      .getOrCreate()
    var shema=StructType(Seq(
      StructField("id",DataTypes.IntegerType),
      StructField("name",DataTypes.StringType),
      StructField("role",DataTypes.StringType),
      StructField("energy",DataTypes.DoubleType),
    ))
    val df1: DataFrame = spark.read.schema(shema).csv("data/battel.txt")
    df1.show()


    //orderBy


    df1.orderBy("energy","id")









  }


}
