package com.galeno.sparksql02

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.types.{DataTypes, StructField, StructType}

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/9/616:08
 */
object 从Row取数据{
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.ERROR)
    val spark = SparkSession.builder()
      .appName("aa")
      .master("local")
      .config("spark.sql.crossJoin.enabled", "true")
      .getOrCreate()
    import spark.implicits._
    import org.apache.spark.sql.functions._  //调用函数里的方法
    var schema = StructType(Seq(
      StructField("id", DataTypes.IntegerType),
      StructField("name", DataTypes.StringType),
      StructField("role", DataTypes.StringType),
      StructField("energy", DataTypes.DoubleType)
    ))
    val df = spark.read.option("header", "true").schema(schema).csv("data/battel2.txt")

   //获取方式1:
    df.rdd.map(row=>{
      row.getAs[Int]("id")
    }).foreach(println)
    //获取方式2:
    df.rdd.map(row=>{
      row.getInt(0)
    }).foreach(println)
    //获取方式3
    df.rdd.map(row=>{
      row.getAs[Int](0)
    }).foreach(println)
    //获取方式4,不指定类型,强转
    df.rdd.map(row=>{
      val id: Any = row.get(0)
      id.asInstanceOf[Int]
    }).foreach(println)
    //获取方式5 模式匹配
    df.rdd.map(row=>{
      row match {
        case Row(id:Int,name:String,role:String,energy:String)=>(id,name,role,energy)
        case _=>(-1,"","",0.0)
      }
    })
    //模式[匹配 偏函数写法
    df.rdd.map {
      case Row(id: Int, name: String, role: String, energy: String) => (id, name, role, energy)
      case _ => (-1, "", "", 0.0)
    }
    //模式[匹配 偏函数写法,再次省略
    df.rdd.map {
      case Row(id: Int, name: String, role: String, energy: String) => (id, name, role, energy)
    }






  }

}
