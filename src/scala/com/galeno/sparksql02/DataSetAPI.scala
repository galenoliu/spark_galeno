package com.galeno.sparksql02

import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/9/616:25
 */
case class Solder(id:Int,name:String,role:String,energy:Double)
object DataSetAPI {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .master("local")
      .appName("招之能来.来之能战,战之必胜")
      .getOrCreate()
    val ds: Dataset[String] = spark.read.option("header", "true").textFile("data/battel.txt")
//    ds.printSchema()
//    ds.show()
val ds1:Dataset[Row] = spark.read.csv("data/battel.txt")
    import spark.implicits._
    val ds2: Dataset[String] = ds1.map(row => {
      row.getString(1)
    })
    //    ds2.show()
    //    ds2.printSchema()
    val ds3: Dataset[String] = spark.read.textFile("data/battel.txt")
    val df3: DataFrame = ds3.toDF()
    val ds22 = ds.map(line => {
      val words = line.split(",")
      (words(0).toInt, words(1), words(2), words(3).toDouble)
    })
    val ds4:Dataset[Row] = ds22.select("_1","_2","_3","_4")

    val ds40: Dataset[Solder] = ds22.map(tp => Solder(tp._1, tp._2, tp._3, tp._4))

    ds40.createTempView("ds40")
    spark.sql(
      """
        |select
        |role,
        |sum(energy) as max_energy
        |
        |from
        |ds40
        |group by
        |role
        |
        |""".stripMargin).show()





  }

}
