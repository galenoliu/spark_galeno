package com.galeno.sparksql

import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/9/49:29
 */
object C01 {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("SparkSQl入门")
      .master("local")
      .getOrCreate()
    //可以从Sparksession中取到sparkContext

    //加载数据文件dataframe
    val df: DataFrame = spark.read.csv("data/battel.txt")
    df.show(100,false)
    val df2 = df.toDF("id", "name", "role", "attack")
    df2.show()
    //查看df的schema 表描述信息
    df2.printSchema()


    //查询所有战斗力大于400的信息记录
    /**
     * API风格表达SQL
     */

    val res1: Dataset[Row] = df2.where("attack>400")
    res1.show()
    //查询每一种角色的平均战斗力

    val res2 = df2.groupBy("role").agg("attack" -> "avg")
    res2.show()


    /**
     * 纯SQL
     */
    df2.createTempView("battel")    //注册一个临时视图
    spark.sql(
      """
        |
        |select
        |role
        |from
        |battel
        |
        |
        |
        |""".stripMargin).show()



  }

}
