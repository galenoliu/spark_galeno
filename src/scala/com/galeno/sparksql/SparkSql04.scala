package com.galeno.sparksql

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/9/421:14
 */
object SparkSql04 {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org.apache").setLevel(Level.WARN)
    val spark = SparkSession.builder()
      .appName("")
      .master("local")
      .getOrCreate()

    /**
     * 从parquet内读取,自带字段描述
     *
     */
    val df1 = spark.read.parquet("data/parquet")
    df1.printSchema()
    df1.show()
    // spark也可以解析orc文件（hive中最常用的列式存储格式文件）
    val df2: DataFrame = spark.read.orc("")


    spark.close()







  }

}
