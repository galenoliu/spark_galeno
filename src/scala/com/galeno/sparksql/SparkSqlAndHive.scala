package com.galeno.sparksql

import org.apache.spark.sql.SparkSession

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/9/422:34
 */
object SparkSqlAndHive {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .master("local")
      .appName("sss")
      .enableHiveSupport()
      .getOrCreate()
//    spark.sql(
//      """
//        |select
//        |t1.id,
//        |t1.name,
//        |t1.gender,
//        |t1.salary
//        |from
//        |(
//        |select *
//        |,rank() over(partition by gender order by salary desc) as num
//        |from stu
//        |)t1 where num <=2
//        |
//        |
//        |
//        |""".stripMargin)

    spark.sql(
      """
        |
        |show tables
        |""".stripMargin).show()







  }

}
