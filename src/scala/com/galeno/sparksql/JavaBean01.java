package com.galeno.sparksql;

import java.io.Serializable;

/**
 * @author galeno
 * @Title:
 * @Description:
 * @date 2021/9/421:52
 */
public class JavaBean01 implements Serializable {
    public JavaBean01(int level, int stuCount, String teacherName) {
        this.level = level;
        this.stuCount = stuCount;
        this.teacherName = teacherName;
    }

    public JavaBean01() {
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getStuCount() {
        return stuCount;
    }

    public void setStuCount(int stuCount) {
        this.stuCount = stuCount;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    private int level;
    private int stuCount;
    private String teacherName;

    @Override
    public String toString() {
        return "JavaBean01{" +
                "level=" + level +
                ", stuCount=" + stuCount +
                ", teacherName='" + teacherName + '\'' +
                '}';
    }
}
