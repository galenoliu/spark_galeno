package com.galeno.sparksql

import scala.beans.BeanProperty

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/9/421:59
 */
class ScalaBean01( @BeanProperty
                   val level: Int,
                   @BeanProperty
                  val stuCount: Int,
                   @BeanProperty
                  val teacherName: String) extends Serializable
