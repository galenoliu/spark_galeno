package com.galeno.sparksql

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/9/421:21
 */
object SparkSql05 {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("taoge").setLevel(Level.WARN)
    val spark = SparkSession.builder()
      .master("local[*]")
      .appName("hh")
      .getOrCreate()


    val df1: DataFrame = spark.read.text("data/wordcount.txt")

    df1.createTempView("wc")
    //wordcount sql实现

    spark.sql(
      """
        |
        |select
        |t2.word,
        |t2.cishu
        |from
        |(
        |select
        |word,
        |count(*) as cishu
        |from
        |(
        |select
        |explode(split(value,"\\s+")) as word
        |from
        |wc) t1
        |group by
        |word ) t2
        |order by cishu desc
        |
        |""".stripMargin).show()







  }

}
