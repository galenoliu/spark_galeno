package com.galeno.sparksql

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/9/422:23
 */
object SparekSql08 {
  /**
   *  * sparksql中有多种表空间
   *    1.  tempView  临时视图表空间
   *    2.  globalTempView 全局临时视图空间  （在多个sparksession之间共享临时视图）
   *    3.  hive元数据空间
   *
   *
   *    如果tempView和hive元数据空间中，有表名相同，则sql查询的是tempView中的
   *    tempView只在创建它的sparksession中可见
   *
   *    globalTempView 在查询时，表名需要加前缀 : select  from global_temp.t_stu
   *    globalTempView在多个sparksession间可以共享
   *
   */
  def main(args: Array[String]): Unit = {
    Logger.getLogger("lll").setLevel(Level.WARN)
    val spark = SparkSession.builder()
      .appName("")
      .master("local[*]")
      .getOrCreate()

    val rdd = spark.sparkContext.makeRDD(Seq(
      (1,"aa"),
      (2,"bb"),
      (3,"cc"),
      (4,"dd")
    ))
    import spark.implicits._
    val df1 = rdd.toDF("id", "name")
    df1.createTempView("stu")
    val spark2 = spark.newSession()
  //  spark2.sql("select * from stu").show() 无法访问spark创建的
    spark.sql("select * from stu").show()
    df1.createGlobalTempView("stug")
    spark2.sql("select * from global_temp.stug").show()


  }

}
