package com.galeno.utils

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/8/2515:52
 */
class SparkUtil {

}
object SparkUtil{
  private var sc = new SparkContext(new SparkConf().setMaster("local[*]").setAppName(this.getClass.getName.substring(0,this.getClass.getName.length-1)))
  def apply(): SparkUtil = new SparkUtil()
  def getSc: SparkContext ={
    sc
  }
  def noLog(): Unit = Logger.getLogger("org").setLevel(Level.WARN)

}
