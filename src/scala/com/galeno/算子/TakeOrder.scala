package com.galeno.算子

import com.galeno.utils.SparkUtil
import org.apache.spark.rdd.RDD

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/9/321:41
 */
object TakeOrder {
  def main(args: Array[String]): Unit = {
    val sc = SparkUtil.getSc
    val rdd1: RDD[(String, Int)] = sc.makeRDD(Seq(("zs", 18), ("ls", 19), ("ls", 22)))
    implicit val ord= Ordering[(String,Int)].on[(String,Int)](tp=>(tp._1,- tp._2))
    val tuples: Array[(String, Int)] = rdd1.takeOrdered(3)

    println(tuples.mkString(","))

  }
}
