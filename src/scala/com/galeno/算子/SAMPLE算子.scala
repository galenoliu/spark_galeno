package com.galeno.算子

import org.apache.spark.{SparkConf, SparkContext}

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/8/2516:39
 */
object SAMPLE算子 {
  def main(args: Array[String]): Unit = {
    val sc = new SparkContext(new SparkConf().setMaster("local").setAppName(this.getClass.getName))
    val rdd1 = sc.parallelize(1 to 10, 2)
    val rdd2 = rdd1.sample(true, 0.5, 1)
    rdd2.collect().foreach(println)

  }

}
