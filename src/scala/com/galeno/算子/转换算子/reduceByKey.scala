package com.galeno.算子.转换算子

import com.galeno.utils.SparkUtil

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/8/2621:28
 * reduceByKey : 把key相同的数据组进行value聚合 ， 元素类型与聚合值类型必须一致
 */
object reduceByKey {
  def main(args: Array[String]): Unit = {
    SparkUtil.noLog()
    val sc = SparkUtil.getSc
    val rdd_rdbk = sc.makeRDD(Seq(("a",1 ), ("b",2), ("a",10)))
    rdd_rdbk.reduceByKey(_+_).foreach(println)




  }

}
