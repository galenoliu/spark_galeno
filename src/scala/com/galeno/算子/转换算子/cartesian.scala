package com.galeno.算子.转换算子

import com.galeno.utils.SparkUtil

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/8/2621:48
 * cartesian 算子  ： 求两个rdd的笛卡尔积
 */
object cartesian {

  def main(args: Array[String]): Unit = {
    val sc = SparkUtil.getSc
    val rdd_carte1 = sc.makeRDD(Seq(("a",1 ), ("b",2)))
    val rdd_carte2 = sc.makeRDD(Seq(("a","10" ),("b","12"), ("c","20")))
    rdd_carte1.cartesian(rdd_carte2).foreach(println)


  }
}
