package com.galeno.算子.转换算子

import com.galeno.utils.SparkUtil

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/8/2621:41
 */
object sortByKey {
  def main(args: Array[String]): Unit = {
    val sc = SparkUtil.getSc
    val rdd_stbk = sc.makeRDD(
      Seq(("a",1 ), ("b",2), ("a",10), ("b",20), ("c",40), ("d",40),("d",18), ("d",30), ("e",10)),
      3
    )
    //按照key
    rdd_stbk.sortByKey(true,2).foreach(println)
    //映射到(kv,null) kv全局有序
    rdd_stbk.map((_,null)).sortByKey(true,2).saveAsTextFile("F:/demo")







  }

}
