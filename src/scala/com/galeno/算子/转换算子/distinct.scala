package com.galeno.算子.转换算子

import com.galeno.utils.SparkUtil
import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/8/2621:11
 */
object distinct {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.ERROR)
    val sc = SparkUtil.getSc
    val rdd_dist: RDD[Int] = sc.makeRDD(List(1,1,3,5,2,3,4,5))
    rdd_dist.distinct().foreach(println)
    println("*"*100)
    rdd_dist.distinct(2).foreach(println)
    println("*"*100)
    rdd_dist.distinct(3).foreach(println)
  }

}
