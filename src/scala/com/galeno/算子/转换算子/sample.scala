package com.galeno.算子.转换算子

import com.galeno.utils.SparkUtil
import org.apache.spark.rdd.RDD

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/8/2620:37
 * 参数1  是否允许对相同元素重复采样
 * 参数2 [0-1]采样比率
 */
object sample {
  def main(args: Array[String]): Unit = {
    val sc = SparkUtil.getSc
    val lst = List(1,2,3,4,5,6,7,8,9,10)
    val rdd1: RDD[Int] = sc.makeRDD(lst, 2)
    val rdd2: RDD[Int] = rdd1.sample(true, 0.4)
    rdd2.foreach(println)




  }

}
