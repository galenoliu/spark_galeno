package com.galeno.算子.转换算子

import com.galeno.utils.SparkUtil
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/8/2620:55
 */
object takeSample {
  def main(args: Array[String]): Unit = {
    val sc: SparkContext = SparkUtil.getSc
    val lst = List(1,2,3,4,5,6,7)
    val rdd1: RDD[Int] = sc.makeRDD(lst, 2)
    val ints: Array[Int] = rdd1.takeSample(false, 10)
    println(ints.mkString("----"))

  }

}
