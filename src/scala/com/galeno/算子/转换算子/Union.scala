package com.galeno.算子.转换算子

import com.galeno.utils.SparkUtil
import org.apache.spark.rdd.RDD

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/8/2621:05
 */
object Union {
  def main(args: Array[String]): Unit = {
    val sc = SparkUtil.getSc
    val rdd1: RDD[(Int, String)] = sc.makeRDD(Seq((1, "a"), (2, "b"), (1, "a")))
    val rdd2: RDD[(Int, String)] = sc.makeRDD(Seq((3, "c"), (2, "d"), (1, "a")))
    val rdd3: RDD[(Int, String)] = rdd1.union(rdd2)
    rdd3.foreach(println)
    println("*"*100)
    val rdd4: RDD[(Int, String)] = rdd2.union(rdd1)
    rdd4.foreach(println)



  }

}
