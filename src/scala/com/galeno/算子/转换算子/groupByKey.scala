package com.galeno.算子.转换算子

import com.galeno.utils.SparkUtil
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/8/2621:15
 */
object groupByKey {

  def main(args: Array[String]): Unit = {

    SparkUtil.noLog()
    var sc: SparkContext = SparkUtil.getSc
    val rdd_bykey: RDD[(Int, String)] = sc.makeRDD(Seq((1, "a"), (2, "b"), (1, "a")))
    rdd_bykey.groupByKey().foreach(println)
    println("*"*100)
    rdd_bykey.groupBy(_._1).foreach(println)





  }


}
