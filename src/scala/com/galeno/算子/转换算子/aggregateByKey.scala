package com.galeno.算子.转换算子

import com.galeno.utils.SparkUtil

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/8/2621:37
 */
object aggregateByKey {
  def main(args: Array[String]): Unit = {
    val sc = SparkUtil.getSc
    val rdd_agbk = sc.makeRDD(Seq(("a","1" ), ("b","2"), ("a","10"), ("b","20")))
    rdd_agbk.aggregateByKey(100)(_-_.toInt,_+_).foreach(println)


  }

}
