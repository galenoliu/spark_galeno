package com.galeno.算子.转换算子

import com.galeno.utils.SparkUtil
import org.apache.spark.rdd.RDD

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/8/2621:08
 * 求交集,并去除重复值
 */
object intersection {
  def main(args: Array[String]): Unit = {
    val sc = SparkUtil.getSc
    val rdd1 = sc.makeRDD(List(1,2,3,4,5))
    val rdd2 = sc.makeRDD(List(2,3,4,6,8,2))
    rdd1.intersection(rdd2).foreach(println)
    println("*"*100)
    rdd2.intersection(rdd1).foreach(println)


  }

}
