package com.galeno.算子.转换算子

import com.galeno.utils.SparkUtil
import org.apache.spark.rdd.RDD

/**
 * @Title: ${file_name}
 * @Description: ${todo}
 * @author galeno
 * @date 2021/8/2621:50
 */
object join {
  def main(args: Array[String]): Unit = {

    val sc = SparkUtil.getSc
    SparkUtil.noLog()

    val rdd_join1 = sc.makeRDD(Seq(("a",1 ), ("a",10), ("b",2), ("e",10)))
    val rdd_join2 = sc.makeRDD(Seq(("a",10 ), ("b",20), ("c",10)))
    println("======内连接===========")
    val rdd_join_res1: RDD[(String, (Int, Int))] = rdd_join1.join(rdd_join2)
    rdd_join_res1.foreach(println)
    println("======左外连接===========")
    val rdd_join_res2: RDD[(String, (Int, Option[Int]))] = rdd_join1.leftOuterJoin(rdd_join2)
    rdd_join_res2.foreach(println)
    println("======全外连接===========")
    val rdd_join_res3: RDD[(String, (Option[Int], Option[Int]))] = rdd_join1.fullOuterJoin(rdd_join2)
    rdd_join_res3.foreach(println)
  }

}
