package com.galeno.demo;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.sources.In;

import java.util.Arrays;
import java.util.Iterator;

/**
 * @author galeno
 * @Title:
 * @Description:
 * @date 2021/8/2921:01
 */
public class JavaCount {
    public static void main(String[] args) {

        SparkConf conf = new SparkConf();
        conf.setAppName("WordConut").setMaster("local[*]");
        JavaSparkContext jsc = new JavaSparkContext(conf);

        JavaRDD<String> rdd1 = jsc.textFile("data/wordcount.txt");
        JavaRDD<String> words = rdd1.flatMap(new FlatMapFunction<String, String>() {

            @Override
            public Iterator<String> call(String s) throws Exception {
                return Arrays.asList(s.split("\\s+")).iterator();
            }
        });




    }
}
